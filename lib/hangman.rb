class Hangman
  attr_reader :guesser, :referee, :board

  def initialize(players)
    @guesser = players[:guesser]
    @referee = players[:referee]
    @board
  end

  def setup
    wordlength = @referee.pick_secret_word
    @guesser.register_secret_length(wordlength)
    @board = Array.new(wordlength)
    # @guesser.register_secret_length(@referee.wordlength)
    # @guesser.register_secret_length()
  end

  def take_turn
    let = @guesser.guess
    @referee.check_guess(let)
  end


end

class HumanPlayer
end

class ComputerPlayer
  # Read File, Select Random Line
  def initialize(dict)
    @dict = dict
    @word
    @predictlength
  end

  def guess(board)
    puts board
    a = gets.chomp

  end

  def pick_secret_word
    @word = @dict.sample
    @word.size
  end

  def wordlength
    @word.size
  end

  def check_guess(let)
    ind = []
    @word.split('').each_with_index do |l, idx|
      if l == let
        ind << idx
      end
    end
    ind
  end

  def register_secret_length(l)
    @predictlength = l
  end

end
